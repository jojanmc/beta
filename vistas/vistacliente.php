<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>


<body>

    <input type="text" id="id" name="id" hidden><br>
    <input type="text" id="nombre" name="nombre" placeholder="Ingresar Nombre"><br>
    <input type="text" id="apellido" name="apellido" placeholder="Ingresar Apellido"><br>
    <input type="text" id="telefono" name="telefono" placeholder="Ingresar telefono"><br>
    <input type="button" id="guardar" value="Guardar" name="guardar" onclick="guardar()">
    <input type="button" hidden id="actualizar" value="Actualizar" name="actualizar" onclick="actualizar()">
    <input type="button"  id="inactivos" value="CLIENTE INACTIVOS" name="inactivos" onclick="inactivos()">
    <input type="button" hidden  id="activos" value="CLIENTE ACTIVOS" name="activos" onclick="activos()">
    
<style>

    #customers,#customers1 {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;

}
#customers1 td, #customers1 th {
  border: 1px solid #ddd;
  padding: 8px;

}






#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers1 tr:nth-child(even){background-color: #f2f2f2;}

#customers1 tr:hover {background-color: #ddd;}



#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;

  
}

#customers1 th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;

  
}
h1{
    text-align: center;
}
</style>

<h1 id="titulo">CLIENTE ACTIVOS</h1>
    <table id="customers">
        
        <thead>
            <tr>
            <th>ID</th>
                <th>NOMBRE</th>
                <th>CEDULA</th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th></th>
                <th></th>

            </tr>
        </thead>

        <tbody>
            <?php
            include('config/conexion.php');

            $sql = "SELECT * FROM cliente WHERE estado=1";
            $resul = mysqli_query($db, $sql);
            while ($fila = mysqli_fetch_array($resul)) {
            ?>


                <tr>
                    <td><?php echo $fila['0']; ?></td>
                    <td><?php echo $fila['1']; ?></td>
                    <td><?php echo $fila['2']; ?></td>
                    <td><?php echo $fila['3']; ?></td>
                    <td><?php echo $fila['4']; ?></td>
                    <td><a onclick="editar('<?php echo $fila['0']; ?>','<?php echo $fila['1']; ?>','<?php echo $fila['2']; ?>','<?php echo $fila['3']; ?>')" href="#">editar</a></td>
                    <td><a onclick="atras()" href="#">atras</a></td>
                </tr>

            <?php
            }
            ?>

        </tbody>
        </table>

        <br>
        <br>
        <h1 hidden id="titulo1">CLIENTES INACTIVOS</h1>
        <table id="customers1" hidden >
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>CEDULA</th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th></th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            <?php
            include('config/conexion.php');

            $sql = "SELECT  FROM cliente WHERE estado=0";
            $resul = mysqli_query($db, $sql);
            while ($fila = mysqli_fetch_array($resul)) {
            ?>


                <tr>
                    <td><?php echo $fila['0']; ?></td>
                    <td><?php echo $fila['1']; ?></td>
                    <td><?php echo $fila['2']; ?></td>
                    <td><?php echo $fila['3']; ?></td>
                    <td><a onclick="editar('<?php echo $fila['0']; ?>','<?php echo $fila['1']; ?>','<?php echo $fila['2']; ?>','<?php echo $fila['3']; ?>')" href="#">editar</a></td>
                    <td><a onclick="atras()" href="#">atras</a></td>
                </tr>

            <?php
            }
            ?>
        </tbody>
    </table>
    



    <script src="jquery-3.5.1.min.js"></script>


    
    <script>
        function editar(id, nombre, apellido, telefono) {
            $('#id').val(id);
            $('#nombre').val(nombre);
            $('#apellido').val(apellido);
            $('#telefono').val(telefono);
            $('#guardar').hide()
            $('#actualizar').show()
            
        }
      function atras(){
        $('#guardar').show()  
        $('#actualizar').hide()
        $('#id').val();
        document.getElementById("nombre").value = "";
        document.getElementById("apellido").value = "";
        document.getElementById("telefono").value = "";


      }

      function inactivos(){
        $('#customers').hide()
        $('#customers1').show()
        
        
        $('#titulo1').show()
        $('#titulo').hide()
        $('#inactivos').hide()
        $('#activos').show()

      }

      function activos(){
        $('#customers').show()
        $('#titulo').show()
        $('#titulo1').hide()
        $('#inactivos').show()
        $('#activos').hide()
        $('#customers1').hide()



      }
      
        
    </script>





    <script>
        function guardar() {
            var nombre = $('#nombre').val()
            var apellido = $('#apellido').val()
            var telefono = $('#telefono').val()

            $.ajax({
                url: 'guardarcliente.php',
                type: 'POST',
                data: {
                    nombre: nombre,
                    apellido: apellido,
                    telefono,
                    telefono
                },
                success: function(e) {
                    alert(e)
                }
            })
            document.getElementById("nombre").value = "";
        document.getElementById("apellido").value = "";
        document.getElementById("telefono").value = "";
        }

        
    </script>

    <script>
        function actualizar() {

            var id = $('#id').val()
            var nombre = $('#nombre').val()
            var apellido = $('#apellido').val()
            var telefono = $('#telefono').val()

            $.ajax({
                url: 'actualizarcliente.php',
                type: 'POST',
                data: {
                    id: id,
                    nombre: nombre,
                    apellido: apellido,
                    telefono,
                    telefono
                },
                success: function(e) {
                    alert(e)
                }
            })

        }
    </script>




</body>

</html>