<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <input type="text" id="id" name="id" placeholder=""><br>
    <div id="contenedor">

        <label>cliente</label>
        <select name="cliente" id="cliente">
            <option value="">seleccione el cliente</option>
        </select><br>


        </select>
        <label id="label">productos</label>
        <select name="producto" id="producto">
            <option value="">seleccione el producto</option>
        </select><br>
        <label id="label">cantidad</label>
        <td><input id="contador" type="number" max="31" min="0" value="0" step="1"></td><br>
        <label id="label">direccion</label>
        <input type="text" id="direccion" name="direccion" placeholder=""><br>
        <label id="label">ciudad</label>
        <input type="text" id="ciudad" name="ciudad" placeholder=""><br>
        <label id="label">telefono</label>
        <input type="text" id="telefono" name="telefono" placeholder=""><br>
    </div>
    <select name="estado" id="estado" hidden>
        <option value="">seleccione el estado</option>
    </select><br>
    <input type="button" value="guardar" id="guardar" onclick="guardarped()">
    <input type="button" value="actualizar" id="actualizar" onclick="actualizarped()" hidden>
    <!-- TABLA INICIAL -->
    <style>
        #customers,
        #customers1 {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td,
        #customers th {
            border: 1px solid #ddd;
            padding: 8px;

        }

        #customers1 td,
        #customers1 th {
            border: 1px solid #ddd;
            padding: 8px;

        }






        #customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers tr:hover {
            background-color: #ddd;
        }

        #customers1 tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers1 tr:hover {
            background-color: #ddd;
        }



        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #04AA6D;
            color: white;


        }

        #customers1 th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }

        h1 {
            text-align: center;
        }
    </style>
    <table id="customers">
        <thead>
            <tr>
                <th>ID</th>
                <th>Producto</th>
                <th>Cliente</th>
                <th>Estado</th>
                <th></th>
                <th>Direccion</th>
                <th>cantidad</th>
                <th>ciudad</th>
                <th>telefono</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
            include('config/conexion.php');
            $sql = "SELECT pedidos.id, producto.nombre,cliente.nombre,  estado.nombre_estado, pedidos.direccion, pedidos.cantidad,pedidos.ciudad,pedidos.telefono 
            FROM pedidos 
            join cliente on pedidos.cliente = cliente.id join producto on pedidos.producto = producto.id_productos join estado on pedidos.estado = estado.id ";
            $resul = mysqli_query($db, $sql);
            while ($fila = mysqli_fetch_array($resul)) {
            ?>
                <tr>
                    <td><?php echo $fila['0']; ?></td>
                    <td><?php echo $fila['1']; ?></td>
                    <td><?php echo $fila['2']; ?></td>
                    <td><?php echo $fila['3']; ?></td>
                    <td><a onclick="seleccionar('<?php echo $fila['0']; ?>','<?php echo $fila['0']; ?>')" href="#">cambiar estado</a></td>
                    <td><?php echo $fila['4']; ?></td>
                    <td><?php echo $fila['5']; ?></td>
                    <td><?php echo $fila['6']; ?></td>
                    <td><?php echo $fila['7']; ?></td>
                    <td><?php echo $fila['8']; ?></td>
                </tr>
                </td </tr>
            <?php
            }
            ?>

        </tbody>

    </table>
    <!-- tabla mostrar pedidos entregados -->
    <!-- <table id="customers">
        <thead>
            <tr>
                <th>ID</th>
                <th>Cliente</th>
                <th>Estado</th>

            </tr>
        </thead>
        <tbody>
            <?php
            include('config/conexion.php');
            $sql = "SELECT  pedidos.id, cliente.nombre, estado.nombre_estado  FROM pedidos 
            join cliente  on pedidos.cliente = cliente.id join estado on pedidos.estado = estado.id  ";
            $resul = mysqli_query($db, $sql);
            while ($fila = mysqli_fetch_array($resul)) {
            ?>
                <tr>
                    <td><?php echo $fila['0']; ?></td>
                    <td><?php echo $fila['1']; ?></td>
                    <td><?php echo $fila['2']; ?></td>

                </tr>
                </td </tr>
            <?php
            }
            ?>

        </tbody>

    </table> -->

    <script src="jquery-3.5.1.min.js"></script>


    <script>
        function guardarped() {
            var cliente = $('#cliente').val()

            var producto = $('#producto').val()
            var direccion = $('#direccion').val()
            var ciudad = $('#ciudad').val()
            var telefono = $('#telefono').val()
            var contador = $('#contador').val()

            $.ajax({
                url: 'guardarpedido.php',
                type: 'POST',
                data: {
                    cliente: cliente,

                    producto: producto,
                    direccion: direccion,
                    ciudad: ciudad,
                    telefono: telefono,
                    contador: contador,


                },
                success: function(e) {
                    alert(e)
                }
            })
        }
    </script>

    <script>
        function actualizarped() {

            var id = $('#id').val()
            var estado = $('#estado').val()


            $.ajax({
                url: 'actualizarestadopedido.php',
                type: 'POST',
                data: {
                    id: id,
                    estado: estado,

                },
                success: function(e) {
                    alert(e)
                }
            })

        }
    </script>
    <script>
        function seleccionar(id) {

            $('#id').val(id)

            $('#contenedor').hide()
            $('#estado').show()
            $('#actualizar').show()







        }



        function listar() {
            $.ajax({
                url: 'listarcliente.php',
                success: function(e) {
                    $("#cliente").append(e)
                }
            });
        }
        listar()

        function listar2() {
            $.ajax({
                url: 'listarproducto.php',
                success: function(e) {
                    $("#producto").append(e)
                }
            });
        }
        listar2()


        function listar1() {
            $.ajax({
                url: 'listarestados.php',
                success: function(e) {
                    $("#estado").append(e)

                }
            });
        }
        listar1()
    </script>



</body>

</html>